const dotenv = require("dotenv");
const mongoose = require("mongoose");

dotenv.config();

mongoose.connect(process.env.MONGODB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

let db = mongoose.connection;

db.once("open", () => console.log("[MongoDB] connected")).on(
  "error",
  console.error.bind(console, "[MongoDB] connection error:")
);

module.exports = db;

const Chat = require("../models/chat");

const { findUser, addUser, userCache } = require("../helpers/userStore");

module.exports = {
  getChatList(req, res) {
    Chat.find({}, "users", function (err, chats) {
      if (err) return handleError(err);
      res.send(chats);
    }).populate("users", "username");
  },

  getChatById(req, res) {
    Chat.findById(req.query.chatId, function (err, chat) {
      if (err) return handleError(err);
      res.send(chat);
    })
      .populate("users", "username")
      .populate({
        path: "messages",
        populate: {
          path: "from",
          select: "username",
        },
      });
  },

  getChatIdByUserId(req, res) {
    Chat.find(
      { users: { $all: [req.query.friendId, req.query.currentUserId] } },
      function (err, chat) {
        if (err) return handleError(err);
        if (chat.length > 0) {
          res.send(chat[0]._id);
        } else {
          Chat.create(
            {
              messages: [],
              users: [req.query.friendId, req.query.currentUserId],
            },
            function (err, chat) {
              if (err) return handleError(err);
              res.send(chat._id);
            }
          );
        }
      }
    );
  },

  addMessageByChatId(req, res) {
    Chat.findById(req.params.chatId, function (err, chat) {
      if (err) return handleError(err);
      chat.messages = [...chat.messages, req.body];
      chat.save().then((updatedChat) => {
        updatedChat
          .populate({
            path: "messages",
            populate: {
              path: "from",
              select: "username",
            },
          })
          .then((finalChat) => {
            /*
              Next we need to broadcast the new message, but it should only be for the relevant users
              AND only if they have an active socket.
            */
            finalChat.users.forEach((user) => {
              const cacheUser = findUser(user._id.toString());
              if (cacheUser) {
                const socket = req.app.get(cacheUser.socketId);
                if (socket) {
                  socket.emit("receiveMessage", finalChat);
                }
              }
            });
            res.send();
          });
      });
    }).populate("users", "username");
  },

  deleteChatById(req, res) {
    Chat.findByIdAndDelete(req.params.chatId, function (err) {
      if (err) return handleError(err);
      res.send({ message: "Chat successfully deleted." });
    });
  },
};

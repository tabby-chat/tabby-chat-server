const User = require("../models/user");

const { findUser, addUser, userCache } = require("../helpers/userStore");

module.exports = {
  loginUser(req, res) {
    User.findById(req.body.userId, function (err, user) {
      if (err) return handleError(err);
      res.send(user);
    });
  },
};

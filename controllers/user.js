const User = require("../models/user");

module.exports = {
  addFriendByUsername(req, res) {
    User.find(
      { username: req.body.friendUsername },
      "username",
      function (err, friend) {
        if (err) return handleError(err);
        User.findById(req.body.currentUserId, function (err, user) {
          const friendId = friend[0]._id.toString();
          if (user.friends.includes(friendId)) {
            res
              .send({
                message: "You already have a friend with that username.",
              })
              .status(409);
          } else {
            user.friends.push(friendId);
            user.save().then(() => {
              res.send({ message: "Friend successfully added." });
            });
          }
        });
      }
    );
  },

  createUser(req, res) {
    User.create(
      { username: req.body.username, friends: [] },
      function (err, user) {
        if (err) return handleError(err);
        res.send(user);
      }
    );
  },

  getUserList(req, res) {
    User.find({}, "username", function (err, users) {
      if (err) return handleError(err);
      res.send(users);
    });
  },

  getUserById(req, res) {
    User.findById(req.params.userId, "username", function (err, user) {
      if (err) return handleError(err);
      res.send(user);
    });
  },

  updateUserById(req, res) {
    User.findByIdAndUpdate(req.params.userId, req.body, function (err) {
      if (err) return handleError(err);
      res.send({ message: "User successfully updated." });
    });
  },

  deleteUserById(req, res) {
    User.findByIdAndDelete(req.params.userId, function (err) {
      if (err) return handleError(err);
      res.send({ message: "User successfully deleted." });
    });
  },

  getFriendListById(req, res) {
    User.findById(req.params.userId, "friends", function (err, user) {
      if (err) return handleError(err);
      res.send(user.friends);
    }).populate("friends", "username");
  },

  getFriendById(req, res) {
    User.findById(req.params.userId, "username", function (err, friend) {
      if (err) return handleError(err);
      res.send(friend);
    });
  },
};

const createError = require("http-errors");

const index = require("./api/index");
const auth = require("./api/auth");
const chat = require("./api/chat");
const user = require("./api/user");

module.exports = (app) => {
  app.use("/api", index);
  app.use("/api/auth", auth);
  app.use("/api/chat", chat);
  app.use("/api/user", user);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404));
  });
};

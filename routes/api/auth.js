var express = require("express");
var router = express.Router();

const AuthController = require("../../controllers/auth");

router.post("/login", AuthController.loginUser);

module.exports = router;

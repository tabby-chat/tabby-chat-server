const express = require("express");
const router = express.Router();

const ChatController = require("../../controllers/chat");

router.get("/list", ChatController.getChatList);
router.get("/byChatId", ChatController.getChatById);
router.get("/byUserId", ChatController.getChatIdByUserId);
router.put("/sendMessage/:chatId", ChatController.addMessageByChatId);
router.delete("/:chatId", ChatController.deleteChatById);

module.exports = router;

const express = require("express");
const router = express.Router();

const UserController = require("../../controllers/user");

router.get("/friends/list/:userId", UserController.getFriendListById);
router.post("/friends/add", UserController.addFriendByUsername);
router.get("/friends/:userId", UserController.getFriendById);
router.get("/list", UserController.getUserList);
router.post("/create", UserController.createUser);
router.get("/:userId", UserController.getUserById);
router.put("/:userId", UserController.updateUserById);
router.delete("/:userId", UserController.deleteUserById);

module.exports = router;

const NodeCache = require("node-cache");
const userCache = new NodeCache();

const addUser = (userData) => {
  const object = {
    socketId: userData.socketId,
    userId: userData.userId,
    connectionTime: new Date(),
  };
  userCache.set(userData.userId, object, 2100);
};

const updateUser = (userId, socketId) => {
  const existing = findUser(userId);
  const object = {
    ...existing,
  };
  object.socketId = socketId;
  userCache.set(userId, object, 2100);
};

const removeUser = (userId) => {
  userCache.del(userId);
};

const findUser = (userId) => {
  return userCache.get(userId);
};

const listUsers = () => {
  const allKeys = userCache.keys();
  let userList = [];
  for (let key of allKeys) {
    userList.push(userCache.get(key));
  }
  return userList;
};

module.exports = {
  addUser,
  removeUser,
  findUser,
  updateUser,
  listUsers,
  userCache,
};

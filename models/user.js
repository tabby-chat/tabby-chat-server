const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    trim: true,
  },
  friends: [{ type: Schema.Types.ObjectId, ref: "user" }],
});

const User = mongoose.model("user", UserSchema);

module.exports = User;

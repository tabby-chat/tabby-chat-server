const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  messages: [
    {
      content: String,
      time: String,
      from: { type: Schema.Types.ObjectId, ref: "user" },
    },
  ],
  users: [{ type: Schema.Types.ObjectId, ref: "user" }],
});

const Chats = mongoose.model("chat", ChatSchema);

module.exports = Chats;

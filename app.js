const cors = require("cors");
const debug = require("debug")("tabby-chat-server:server");
const express = require("express");
const http = require("http");
const logger = require("morgan");
const socketio = require("socket.io");

const router = require("./routes/router");

const { addUser, userCache } = require("./helpers/userStore");

require("./config/db");

var app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

router(app);

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500);
});

var port = normalizePort(process.env.PORT || "8080");
app.set("port", port);

var server = http.createServer(app);
var io = socketio(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

io.on("connect", (socket) => {
  app.set(`${socket.id}`, socket);

  socket.on("updateSocket", ({ socketId, userId }) => {
    if (socketId && userId) {
      addUser({
        socketId,
        userId,
      });
    }
  });
});

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 *
 */
function normalizePort(val) {
  var port = parseInt(val, 10);
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
}

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }
  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);
}
